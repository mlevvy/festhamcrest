package pl.org.kata.test.festhamcrest.strings;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pl.org.kata.test.festhamcrest.BookService;
import pl.org.kata.test.festhamcrest.IBookService;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * User: Michał Lewandowski
 * Date: 19.05.2013
 * Time: 13:33
 */
public class HamcrestStringTest {
    private IBookService sut;

    @BeforeClass
    public void setUp() {
        sut = new BookService();
    }

    /*
     * Method sut.getLibraryName() should not be null or empty .
     */
    @Test
    public void shouldNotBeNullOrEmpty() {
        assertThat(sut.getLibraryName(), both(not("")).and(notNullValue()));
    }

    /*
     * Method sut.getLibraryName() should equal ignoring case to "ultimate jug jvm library" .
     */
    @Test
    public void shouldBeEqualIgnoringCase() {
        assertThat(sut.getLibraryName(), equalToIgnoringCase("ultimate jug jvm library"));
    }

    /*
     * Method sut.getLibraryName() should equal to "Ultimate JUG JVM Library" .
     */
    @Test
    public void shouldBeEqual() {
        assertThat(sut.getLibraryName(), equalTo("Ultimate JUG JVM Library"));
    }

    /*
     * Method result sut.getLibraryName() should contains "JVM" .
     */
    @Test
    public void shouldContainsJVM() {
        assertThat(sut.getLibraryName(), containsString("JVM"));
    }

    /*
     * Method result sut.getLibraryName() should not contains "C#" .
     */
    @Test
    public void shouldNotContainCSharp() {
        assertThat(sut.getLibraryName(), not(containsString("C#")));
    }

    /*
     * Method result sut.getLibraryName() should ends with "Library" .
     */
    @Test
    public void shouldEndsWithLibrary() {
        assertThat(sut.getLibraryName(), endsWith("Library"));
    }

    /*
     * Method result sut.getLibraryName() should has size 24 .
     */
    @Test
    public void shouldHaveSize24() {
        assertThat(sut.getLibraryName().length(), is(24));
    }

    /*
     * Method result sut.findBook(2) should return null or empty .
     */
    @Test(enabled = true)
    public void titleShouldBeNullOrEmpty() {
        assertThat(sut.findBook(2), either(is("")).or(nullValue()));
        assertThat(sut.findBook(2), anyOf(is(""), (nullValue())));
        assertThat(sut.findBook(2), isEmptyOrNullString());
    }

}
