package pl.org.kata.test.festhamcrest.custom;

import org.fest.assertions.core.Condition;
import pl.org.kata.test.festhamcrest.Book;

/**
 * User: Michał Lewandowski
 * Date: 19.05.2013
 * Time: 14:54
 */
public class CustomFestCondition extends Condition<Book> {
    @Override
    public boolean matches(Book value) {
        if(value == null){
            return false;
        }

        if(value.getAuthor() == null || value.getAuthor().isEmpty()){
            return false;
        }

        if(value.getPages() == null || value.getPages() <= 0){
            return false;
        }

        if(value.getTitle() == null ){
            return false;
        }
        return true;
    }

    public static CustomFestCondition valid(){
        return new CustomFestCondition();
    }
}
