package pl.org.kata.test.festhamcrest.custom;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import pl.org.kata.test.festhamcrest.Book;

/**
 * User: Michał Lewandowski
 * Date: 20.05.2013
 * Time: 19:24
 */
public class CustomHamcrestMatcher extends TypeSafeMatcher<Book> {

    @Override
    protected boolean matchesSafely(Book book) {
        if (book == null) {
            return false;
        }

        if (book.getAuthor() == null || book.getAuthor().isEmpty()) {
            return false;
        }

        if (book.getPages() == null || book.getPages() <= 0) {
            return false;
        }

        if (book.getTitle() == null) {
            return false;
        }
        return true;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("valid book");
    }

    @Factory
    public static Matcher<Book> validBook() {
        return new CustomHamcrestMatcher();
    }

}
